use std::env;

pub fn export_features(prefix:&str) {
    for (key, _value) in env::vars() {
        if key.starts_with(prefix) && key.len() > prefix.len(){
            let feature_name = &key[prefix.len()..];
            println!("cargo:rustc-cfg={}", feature_name);
        }
    }
}

pub fn export_c_flags(env_name:&str) {
    if let Ok(v) = std::env::var(env_name) {
        println!("cargo:warning={}={}", env_name, v);
        for item in v.split(" ") {
            if item.is_empty() || !item.starts_with("-D"){
                continue;
            }
            let item = &item[2..];
            println!("cargo:warning={}", item);
            let sub_items:Vec<&str> = item.split("=").collect();
            if sub_items.len() < 2 {
                continue;
            }
            let k = sub_items[0];
            let v = sub_items[1];
            let v = if v.starts_with("\"") {
                v.to_owned()
            } else {
                format!("\"{}\"", v)
            };
            println!("cargo:rustc-cfg={}={}", k, v);
        }
    }
}