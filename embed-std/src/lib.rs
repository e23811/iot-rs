#![cfg_attr(feature = "no_std", no_std)]
#![feature(negative_impls)]
#![feature(c_variadic)]
#![allow(dead_code)]

extern crate alloc;

#[macro_use]
extern crate lazy_static;

#[macro_use]
pub mod sys_log;

#[macro_use]
pub mod macros;

pub mod allocator;
pub mod errors;
pub mod ffi;
pub(crate) mod porting;
mod prelude;
pub mod sync;
pub mod thread;
pub mod time;
pub mod util;

#[cfg(feature = "f-log")]
mod loglib;

pub mod str_util;

pub use prelude::*;

pub use libc;

#[cfg(feature = "f-log")]
pub use log;

#[cfg(feature = "f-log")]
pub use loglib::init_console_logger;
