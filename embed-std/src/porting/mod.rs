#[cfg(feature = "liteos")]
mod liteos;
#[cfg(feature = "liteos")]
pub use liteos::*;

#[cfg(feature = "linux")]
mod linux;

#[cfg(feature = "linux")]
pub use linux::*;
