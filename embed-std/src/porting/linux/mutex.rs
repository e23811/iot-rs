use crate::Error;
use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut};
use core::ptr::null_mut;

pub type LockResult<Guard> = Result<Guard, Error>;

pub struct Mutex<T: ?Sized> {
    mutex_id: libc::pthread_mutex_t,
    data: UnsafeCell<T>,
}

unsafe impl<T: ?Sized + Send> Send for Mutex<T> {}
unsafe impl<T: ?Sized + Send> Sync for Mutex<T> {}

impl<T> Mutex<T> {
    pub fn new(data: T) -> Self {
        let mut my = Self {
            mutex_id: unsafe { core::mem::zeroed() },
            data: UnsafeCell::new(data),
        };
        unsafe { libc::pthread_mutex_init(&mut my.mutex_id, null_mut()) };
        my
    }
}

impl<T: ?Sized> Mutex<T> {
    pub fn lock(&self) -> LockResult<MutexGuard<'_, T>> {
        self.lock_internal();
        unsafe { MutexGuard::new(self) }
    }

    fn lock_internal(&self) {
        unsafe {
            libc::pthread_mutex_lock(
                &self.mutex_id as *const libc::pthread_mutex_t as *mut libc::pthread_mutex_t,
            );
        };
    }

    pub fn unlock(guard: MutexGuard<'_, T>) {
        drop(guard);
    }

    fn unlock_internal(&self) {
        unsafe {
            libc::pthread_mutex_unlock(
                &self.mutex_id as *const libc::pthread_mutex_t as *mut libc::pthread_mutex_t,
            );
        };
    }

    pub(crate) unsafe fn id(&self) -> *mut libc::pthread_mutex_t {
        &self.mutex_id as *const libc::pthread_mutex_t as *mut libc::pthread_mutex_t
    }
}

impl<T: ?Sized> Drop for Mutex<T> {
    fn drop(&mut self) {
        unsafe {
            libc::pthread_mutex_destroy(&mut self.mutex_id);
        }
    }
}

pub struct MutexGuard<'a, T: ?Sized + 'a> {
    lock: &'a Mutex<T>,
}

impl<T: ?Sized> !Send for MutexGuard<'_, T> {}
unsafe impl<T: ?Sized + Sync> Sync for MutexGuard<'_, T> {}

impl<'mutex, T: ?Sized> MutexGuard<'mutex, T> {
    unsafe fn new(lock: &'mutex Mutex<T>) -> LockResult<MutexGuard<'mutex, T>> {
        Ok(Self { lock })
    }
}

impl<T: ?Sized> Deref for MutexGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<T: ?Sized> DerefMut for MutexGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<T: ?Sized> Drop for MutexGuard<'_, T> {
    #[inline]
    fn drop(&mut self) {
        self.lock.unlock_internal();
    }
}
