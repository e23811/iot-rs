use super::mutex::Mutex;

pub enum Ordering {
    Relaxed,
    Release,
    Acquire,
    AcqRel,
    SeqCst,
}

//@TODO optimize
pub struct AtomicUsize {
    seq: Mutex<usize>,
}

impl AtomicUsize {
    pub fn new(n: usize) -> Self {
        Self { seq: Mutex::new(n) }
    }
    pub fn load(&self, _order: Ordering) -> usize {
        let inner = self.seq.lock().unwrap();
        let seq = *inner;
        seq
    }

    pub fn store(&self, val: usize, _order: Ordering) {
        let mut inner = self.seq.lock().unwrap();
        *inner = val;
    }

    pub fn fetch_add(&self, val: usize, _order: Ordering) -> usize {
        let mut inner = self.seq.lock().unwrap();
        let seq = *inner;
        *inner += val;
        seq
    }

    pub fn fetch_sub(&self, val: usize, _order: Ordering) -> usize {
        let mut inner = self.seq.lock().unwrap();
        let seq = *inner;
        *inner -= val;
        seq
    }
}
