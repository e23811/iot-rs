use super::ffi::{osKernelGetTickCount, osKernelGetTickFreq};
use core::time::Duration;

pub fn now() -> Duration {
    unsafe {
        Duration::from_millis(osKernelGetTickCount() as u64 * 1000 / osKernelGetTickFreq() as u64)
    }
}
