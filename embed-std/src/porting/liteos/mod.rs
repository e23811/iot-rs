mod ffi;
// for no atomic arch, i.e risc-v imc
pub mod atomic;
pub mod channel;
pub mod mutex;
pub mod thread;
pub mod time;

pub use ffi::{free, malloc, printf};
