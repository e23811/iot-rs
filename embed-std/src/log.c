#include <stdio.h>
#include <stdarg.h>

extern void write_log_rust(unsigned char level, const char *module, unsigned int module_len, unsigned int line,
                           const char *data, unsigned int data_len);

void es_write_log(unsigned char level, const char *module, const char *func_name, unsigned int line, const char *fmt, ...)
{
    va_list args;
    char module_name[128] = {0};
    char data[512] = {0};
    va_start(args, fmt);
    unsigned int data_len = vsnprintf(data, sizeof(data)-1, fmt, args);
    va_end(args);
    unsigned int module_len = snprintf(module_name, sizeof(module_name), "%s::%s", module, func_name);
    if(data_len > sizeof(data) || module_len > sizeof(module_name)) {
//        printf("%s:%d: bug!\n", __FUNCTION__ , __LINE__);
        return;
    }
    write_log_rust(level, module_name, module_len, line, data, data_len);
}

extern void set_iot_rust_log_level(unsigned char level);
void es_set_log_level(unsigned char level)
{
    set_iot_rust_log_level(level);
}

extern unsigned char get_iot_rust_log_level();
unsigned char es_get_log_level()
{
    return get_iot_rust_log_level();
}
