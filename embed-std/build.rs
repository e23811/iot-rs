fn main() {
    println!("cargo:rerun-if-changed=src/log.c");
    let mut compiler = cc::Build::new();

    println!("target {:?}", std::env::var("TARGET"));
    if cfg!(target = "armv7-unknown-linux-uclibceabihf") {
        if let Ok(v) = std::env::var("CC") {
            if let Some(pos) = v.find("-gcc") {
                let comp = &v[..pos];
                println!("compiler prefix {}", comp);
                compiler.compiler(comp);
            }
        }
        // compiler.compiler("armv7-unknown-linux-uclibceabihf");
    }

    compiler.file("src/log.c").compile("liblog_c.a");
}
