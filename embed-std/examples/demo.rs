use embed_std;
use embed_std::init_console_logger;
use embed_std::log::{debug, error, info, warn};

fn main() {
    init_console_logger();
    debug!("debug : ");
    info!("info : {}", "info");
    warn!("warn : {}", "warn");
    error!("error : {}", "error");
}
