#ifndef _EMBED_STD_H_
#define _EMBED_STD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define ES_LOG_LEVEL_OFF 0
#define ES_LOG_LEVEL_DEBUG 1
#define ES_LOG_LEVEL_INFO 2
#define ES_LOG_LEVEL_WARN 3
#define ES_LOG_LEVEL_ERROR 4
#define ES_LOG_LEVEL_PASS 5

/**
 * env:
 * ES_LOG_COLOR = 1 | on    : log color enabled
 * ES_LOG = off | debug | info | warn | error | pass
 */
void es_detect_env_log_level();

void es_set_log_level(unsigned char level);

unsigned char es_get_log_level();

void es_enable_log_color(unsigned  char enable);
/**
 *
 * @param level
 * @param module: module name
 * @param func_name: file name
 * @param line: line in file
 * @param fmt
 * @param ...
 */
void es_write_log(unsigned char level, const char *module, const char *func_name, unsigned int line, const char *fmt, ...);

#define ALL_MODULE ""

#define ESM_DEBUG(module, fmt...) es_write_log(ES_LOG_LEVEL_DEBUG, module, __FUNCTION__, __LINE__, fmt)
#define ESM_INFO(module, fmt...) es_write_log(ES_LOG_LEVEL_INFO, module, __FUNCTION__, __LINE__, fmt)
#define ESM_WARN(module, fmt...) es_write_log(ES_LOG_LEVEL_WARN, module, __FUNCTION__, __LINE__, fmt)
#define ESM_ERROR(module, fmt...) es_write_log(ES_LOG_LEVEL_ERROR, module, __FUNCTION__, __LINE__, fmt)
#define ESM_PASS(module, fmt...) es_write_log(ES_LOG_LEVEL_PASS, module, __FUNCTION__, __LINE__, fmt)

#define ES_DEBUG(fmt...) es_write_log(ES_LOG_LEVEL_DEBUG, ALL_MODULE, __FUNCTION__, __LINE__, fmt)
#define ES_INFO(fmt...) es_write_log(ES_LOG_LEVEL_INFO, ALL_MODULE, __FUNCTION__, __LINE__, fmt)
#define ES_WARN(fmt...) es_write_log(ES_LOG_LEVEL_WARN, ALL_MODULE, __FUNCTION__, __LINE__, fmt)
#define ES_ERROR(fmt...) es_write_log(ES_LOG_LEVEL_ERROR, ALL_MODULE, __FUNCTION__, __LINE__, fmt)
#define ES_PASS(fmt...) es_write_log(ES_LOG_LEVEL_PASS, ALL_MODULE, __FUNCTION__, __LINE__, fmt)

#ifdef __cplusplus
};
#endif

#endif //_EMBED_STD_H_

