#include "embed_std.h"

int main(int argc, char *argv[]) {
    es_detect_env_log_level();
    // es_enable_log_color(1);
    // es_set_log_level(ES_LOG_LEVEL_DEBUG);
    ES_INFO("es info");
    ESM_INFO("main", "for color, export ES_LOG_COLOR=on");
    ESM_INFO("main", "log level %d", es_get_log_level());
    ESM_DEBUG("main", "number %d", 10);
    ESM_INFO("", "str %s", "ok");
    ESM_WARN("EVENT", "float %.3f", 10.123456);
    ESM_ERROR("WIFI", "bool %d", 1);
    ESM_PASS("test", "pass %s test", "audio");
    return 0;
}
