use core::time::Duration;
use embed_std::infoln;
use net::{Events, Interest, Poll, Token, UdpSocket};

fn main() {
    infoln!("hello");
    let mut socket = UdpSocket::bind("127.0.0.1:12345".parse().unwrap()).unwrap();

    let mut poll = Poll::new().unwrap();
    const SENDER: Token = Token(0);
    const ECHOER: Token = Token(1);
    poll.registry()
        .register(&mut socket, SENDER, Interest::WRITABLE | Interest::READABLE)
        .unwrap();
    let mut events = Events::with_capacity(128);
    // Receives a single datagram message on the socket. If `buf` is too small to hold
    // the message, it will be cut off.
    let mut buf = [0; 1500];
    // socket.set_nonblocking(false).unwrap();
    loop {
        poll.poll(&mut events, Some(Duration::from_millis(100)))
            .unwrap();
        for event in events.iter() {
            match event.token() {
                SENDER => {
                    socket.send(&buf);
                }
                ECHOER => {
                    let (amt, src) = socket.recv_from(&mut buf).unwrap();
                    socket.send_to(&buf, src).unwrap();
                }
                _ => unreachable!(),
            }
        }
    }
}
