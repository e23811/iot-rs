use alloc::string::String;

pub const SEND_BUFFER_SIZE: usize = 2048;
pub const RECV_BUFFER_SIZE: usize = 2048;

pub const PROTO_FLAGS_ENCRYPT_MASK: u8 = 0x80;
pub const PROTO_FLAGS_COMPRESS_MASK: u8 = 0x40;
pub const PROTO_FLAGS_VER_MASK: u8 = 0x3F;

pub const DOMAIN: &str = "sys";
pub const DEVICE_SERVICE: &str = "device";
pub const GSLB_SERVICE: &str = "gslb";
pub const OTA_SERVICE: &str = "gslb";
pub const EPRO_APP_SERVICE: &str = "eApp";

#[derive(Debug)]
pub enum ServerType {
    Unknown,
    DeviceGateway,
    ApiGateway,
    Ntp,
    Ota,
}

impl From<u8> for ServerType {
    fn from(_: u8) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub enum ProtoType {
    Unknown,
    Http,
    Https,
    Mqtt,
    Mqtts,
    Ntp,
}

impl From<u8> for ProtoType {
    fn from(_: u8) -> Self {
        todo!()
    }
}

pub struct ServerAddress {
    pub ip: String,
    pub domain: String,
    pub server_type: ServerType,
    pub proto_type: ProtoType,
    pub priority: u8,
}

#[derive(Debug)]
pub struct OtaVersion {
    pub version: String,
    pub force_upgrade: bool,
    pub release_notes: String,
    pub download_url: String,
    pub check_sum: String,
}
