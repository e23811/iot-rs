use crate::Error;
use alloc::format;
use embed_std::errorln;

pub fn map_rmpv_write_err(e: rmpv::encode::Error) -> Error {
    Error::Other(format!("{:?}", e))
}

pub fn map_rmpv_read_err(e: rmpv::decode::Error) -> Error {
    Error::Other(format!("{:?}", e))
}

pub fn log_err(e: Error) {
    errorln!("{}", e);
}
