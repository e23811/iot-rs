use crate::model::{CommonValue, NamedValue, Prop, Value};
use crate::Result;

impl Prop {
    pub fn deserialize_msgpack(&self, _buff: &[u8]) -> Result<usize> {
        Ok(0)
    }
}

impl NamedValue {
    pub fn deserialize_msgpack(&self, _buff: &[u8]) -> Result<usize> {
        Ok(0)
    }
}

impl CommonValue {
    pub fn deserialize_msgpack(&self, _buff: &[u8]) -> Result<usize> {
        Ok(0)
    }
}

impl Value {
    pub fn deserialize_msgpack(&self, _buff: &[u8]) -> Result<usize> {
        Ok(0)
    }
}
