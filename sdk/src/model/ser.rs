use crate::model::{CommonValue, NamedValue, Prop, Value};
use crate::Result;

impl Prop {
    pub fn serialize_msgpack(&self, _buff: &mut [u8]) -> Result<usize> {
        Ok(0)
    }
}

impl NamedValue {
    pub fn serialize_msgpack(&self, _buff: &mut [u8]) -> Result<usize> {
        Ok(0)
    }
}

impl CommonValue {
    pub fn serialize_msgpack(&self, _buff: &mut [u8]) -> Result<usize> {
        Ok(0)
    }
}

impl Value {
    pub fn serialize_msgpack(&self, _buff: &mut [u8]) -> Result<usize> {
        Ok(0)
    }
}
