use chrono::{DateTime, Local};
use std::path::Path;
use std::{env, fs};

fn main() {
    let local: DateTime<Local> = Local::now();
    let build_time = local.format("%Y-%m-%d %H:%M:%S").to_string();
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("build_info.rs");
    fs::write(
        &dest_path,
        format!("const BUILD_TIME:&str=\"{}\";", build_time),
    )
    .unwrap();
}
