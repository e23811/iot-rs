#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "iot_api.h"

#define TEST_UPLOAD 1
#if TEST_UPLOAD
void test_upload() {
    const char *schema = "http";
    const char *host = "cn.api.iot.zediel.cn";
//    const char *host = "172.18.100.202:20048";
    const char *device_uid = "114LYDA0CSF1F10";
    const char *secret = "1k43knoaci5izah9ksos816gjyuh4ivj";
    struct upload_context *ctx = create_upload_context(schema, host, device_uid, secret);
    if(ctx) {
        const char *key = "all_day_mp4/2023/06/15/11/2023-06-15-11_50_00.png";
        const char *filename = "test.jpeg";
        upload_response_t resp = {0};
        int ret = ctx->upload(ctx, key, filename, &resp);
        printf("upload %s %s return %d %s\n", key, filename, ret, ret == 0 ? "ok" : "failed");
        if(ret == 0) {
            printf("url: %s\n", resp.url);
        }
        ctx->free(ctx);
    }
}
#endif

void dump_servers(server_address_t *servers, int count)
{
    int i;
    for(i=0; i< count; i++) {
        printf("server[%d]       ip: %s\n", i, servers[i].ip);
        printf("server[%d]   domain: %s\n", i, servers[i].domain);
        printf("server[%d]    stype: %d\n", i, servers[i].server_type);
        printf("server[%d]    ptype: %d\n", i, servers[i].proto_type);
        printf("server[%d] priority: %d\n", i, servers[i].priority);
    }
}

int main(int argc, char *argv[]) {
    extern void detect_env_log_level();
    detect_env_log_level();

    server_address_t servers[6];
    memset(&servers, 0, sizeof(servers));
    uint8_t act = 0;
    uint32_t ret = get_servers_from_gslb(NULL, "module38", "114L7QASH74VK8C", servers, sizeof(servers)/sizeof(server_address_t), &act);
    if(ret != 0) {
        printf("ret %d\n", ret);
        return 1;
    }
    dump_servers(servers, act);
    act = 0;
    ret = get_servers_from_gslb_v2(NULL, "module38", "114L7QASH74VK8C", servers, sizeof(servers)/sizeof(server_address_t), &act, 0x4);
    if(ret != 0) {
        printf("ret %d\n", ret);
        return 1;
    }
    dump_servers(servers, act);

#ifdef TEST_UPLOAD
    test_upload();
#endif
    return 0;
}