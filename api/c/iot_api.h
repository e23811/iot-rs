#ifndef _IOT_API_H_
#define _IOT_API_H_ 1

#include "iot_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

const char * get_iot_api_version();

iot_err_code_e get_servers_from_gslb(const char *host,
                                     const char *product_code,
                                     const char *device_uid,
                                     server_address_t *addrs,
                                     uint8_t count,
                                     uint8_t *act_count);

/**
 * @param host
 * @param product_code
 * @param device_uid
 * @param addrs
 * @param count
 * @param act_count
 * @param flags: 0(all), 0x1(api gw), 0x2(dev gw), 0x4(keep gw)
 * @return
 */
iot_err_code_e get_servers_from_gslb_v2(const char *host,
                                     const char *product_code,
                                     const char *device_uid,
                                     server_address_t *addrs,
                                     uint8_t count,
                                     uint8_t *act_count,
                                     uint32_t flags);

typedef struct {
    char url[256];
} upload_response_t;

struct upload_context {
    void *priv;
    void (*free)(struct upload_context *ctx);
    void (*set_timeout)(struct upload_context *ctx, unsigned int timeout);
    iot_err_code_e (*upload)(struct upload_context *ctx, const char *key, const char *filename, upload_response_t *result);
};


struct upload_context * create_upload_context(const char *api_schema, const char *api_host,
                                                const char *device_uid, const char *secret);

typedef void (*download_progress_cb_t)(long long total, long long pos, void *arg);
iot_err_code_e download_file(const char *url, const char *filename, download_progress_cb_t cb, void *cb_arg);

iot_err_code_e gen_d_access_token(const char *device_uid, const char *secret, char *token, unsigned int token_len);

void auto_detect_log();

#ifdef __cplusplus
};
#endif

#endif /* _IOT_API_H_ */