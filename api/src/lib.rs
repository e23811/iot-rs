#![no_std]
#![allow(dead_code)]

extern crate alloc;

use embed_std::cstr;

#[allow(non_camel_case_types)]
#[allow(non_upper_case_globals)]
pub mod capi;

pub mod gslb;
pub mod macros;

#[cfg(feature = "storage")]
pub mod storage;

pub mod util;
pub mod utils;

#[no_mangle]
fn get_iot_api_version() -> *const embed_std::libc::c_char {
    cstr!(concat!(env!("CARGO_PKG_VERSION"), "\0"))
}
