#[macro_export]
macro_rules! try_curl {
    ($stat:expr) => {
        $stat.map_err($crate::util::map_curl_error_to_std_err)
    };
}

#[macro_export]
macro_rules! try_curl_form {
    ($stat:expr) => {
        $stat.map_err($crate::util::map_curl_form_error_to_std_err)
    };
}

#[macro_export]
macro_rules! try_curl_multi {
    ($stat:expr) => {
        $stat.map_err($crate::util::map_curl_multi_error_to_std_err)
    };
}

#[macro_export]
macro_rules! try_string_err {
    ($stat:expr) => {
        $stat.map_err($crate::util::map_string_error_to_std_err)
    };
}
