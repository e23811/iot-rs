#!/bin/bash


. ~/bin/t31.sh
export TARGET_CC=mips-linux-uclibc-gnu-gcc
export TARGET_AR=mips-linux-uclibc-gnu-ar
export OPENSSL_DIR=/home/armers/projects/embed/t31/src/ipc_sdk
xargo build -Z build-std --release --target mipsel-unknown-linux-uclibc
