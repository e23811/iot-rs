#![allow(dead_code)]
#![allow(deprecated)]
#![feature(linked_list_remove)]

#[cfg(feature = "capi")]
#[allow(non_camel_case_types)]
pub mod capi;

#[cfg(feature = "capi")]
#[allow(non_camel_case_types)]
pub mod cqdispatcher;

pub mod qbus;
pub mod qconsumer;
pub mod qdispatcher;
pub mod qmessage;

extern crate alloc;
extern crate core;

pub use embed_std;
