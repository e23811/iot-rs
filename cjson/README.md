[TOC]

demos(with gslb)

`xargo build --release`

|       |  serde_json |  cjson |
| ------ | ---------- | ------- |
| binary size   |  70KB(43KB)  |       |
| rss memory   |    |   64KB    |

- serde_json: 762K
```
text	   data	    bss	    dec	    hex	filename
 553725	  15448	   1489	 570662	  8b526	demos
```
```
 RSS    SHM
 1640   1440
```
- cJson: 692K
```
text	   data	    bss	    dec	    hex	filename
 510812	  14480	   1521	 526813	  809dd	demos
```
```
 RSS    SHM
 1704   1500
```
