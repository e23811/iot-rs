use core::prelude::rust_2018::derive;
use core::clone::Clone;
use core::marker::Copy;
/* automatically generated by rust-bindgen 0.64.0 */

pub const CJSON_VERSION_MAJOR: u32 = 1;
pub const CJSON_VERSION_MINOR: u32 = 7;
pub const CJSON_VERSION_PATCH: u32 = 13;
pub const cJSON_Invalid: u32 = 0;
pub const cJSON_False: u32 = 1;
pub const cJSON_True: u32 = 2;
pub const cJSON_NULL: u32 = 4;
pub const cJSON_Number: u32 = 8;
pub const cJSON_String: u32 = 16;
pub const cJSON_Array: u32 = 32;
pub const cJSON_Object: u32 = 64;
pub const cJSON_Raw: u32 = 128;
pub const cJSON_IsReference: u32 = 256;
pub const cJSON_StringIsConst: u32 = 512;
pub const CJSON_NESTING_LIMIT: u32 = 1000;
pub type wchar_t = ::core::ffi::c_int;
#[repr(C)]
#[repr(align(16))]
#[derive(Debug, Copy, Clone)]
pub struct max_align_t {
    pub __clang_max_align_nonce1: ::core::ffi::c_longlong,
    pub __bindgen_padding_0: u64,
    pub __clang_max_align_nonce2: u128,
}
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct cJSON {
    pub next: *mut cJSON,
    pub prev: *mut cJSON,
    pub child: *mut cJSON,
    pub type_: ::core::ffi::c_int,
    pub valuestring: *mut ::core::ffi::c_char,
    pub valueint: ::core::ffi::c_int,
    pub valuedouble: f64,
    pub string: *mut ::core::ffi::c_char,
}
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct cJSON_Hooks {
    pub malloc_fn:
        ::core::option::Option<unsafe extern "C" fn(sz: usize) -> *mut ::core::ffi::c_void>,
    pub free_fn: ::core::option::Option<unsafe extern "C" fn(ptr: *mut ::core::ffi::c_void)>,
}
pub type cJSON_bool = ::core::ffi::c_int;
extern "C" {
    pub fn cJSON_Version() -> *const ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_InitHooks(hooks: *mut cJSON_Hooks);
}
extern "C" {
    pub fn cJSON_Parse(value: *const ::core::ffi::c_char) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_ParseWithLength(
        value: *const ::core::ffi::c_char,
        buffer_length: usize,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_ParseWithOpts(
        value: *const ::core::ffi::c_char,
        return_parse_end: *mut *const ::core::ffi::c_char,
        require_null_terminated: cJSON_bool,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_ParseWithLengthOpts(
        value: *const ::core::ffi::c_char,
        buffer_length: usize,
        return_parse_end: *mut *const ::core::ffi::c_char,
        require_null_terminated: cJSON_bool,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_Print(item: *const cJSON) -> *mut ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_PrintUnformatted(item: *const cJSON) -> *mut ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_PrintBuffered(
        item: *const cJSON,
        prebuffer: ::core::ffi::c_int,
        fmt: cJSON_bool,
    ) -> *mut ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_PrintPreallocated(
        item: *mut cJSON,
        buffer: *mut ::core::ffi::c_char,
        length: ::core::ffi::c_int,
        format: cJSON_bool,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_Delete(item: *mut cJSON);
}
extern "C" {
    pub fn cJSON_GetArraySize(array: *const cJSON) -> ::core::ffi::c_int;
}
extern "C" {
    pub fn cJSON_GetArrayItem(array: *const cJSON, index: ::core::ffi::c_int) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_GetObjectItem(
        object: *const cJSON,
        string: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_GetObjectItemCaseSensitive(
        object: *const cJSON,
        string: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_HasObjectItem(
        object: *const cJSON,
        string: *const ::core::ffi::c_char,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_GetErrorPtr() -> *const ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_GetStringValue(item: *mut cJSON) -> *mut ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_GetNumberValue(item: *mut cJSON) -> f64;
}
extern "C" {
    pub fn cJSON_IsInvalid(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsFalse(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsTrue(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsBool(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsNull(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsNumber(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsString(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsArray(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsObject(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_IsRaw(item: *const cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_CreateNull() -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateTrue() -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateFalse() -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateBool(boolean: cJSON_bool) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateNumber(num: f64) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateString(string: *const ::core::ffi::c_char) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateRaw(raw: *const ::core::ffi::c_char) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateArray() -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateObject() -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateStringReference(string: *const ::core::ffi::c_char) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateObjectReference(child: *const cJSON) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateArrayReference(child: *const cJSON) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateIntArray(
        numbers: *const ::core::ffi::c_int,
        count: ::core::ffi::c_int,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateFloatArray(numbers: *const f32, count: ::core::ffi::c_int) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateDoubleArray(numbers: *const f64, count: ::core::ffi::c_int)
        -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_CreateStringArray(
        strings: *const *const ::core::ffi::c_char,
        count: ::core::ffi::c_int,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddItemToArray(array: *mut cJSON, item: *mut cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_AddItemToObject(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
        item: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_AddItemToObjectCS(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
        item: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_AddItemReferenceToArray(array: *mut cJSON, item: *mut cJSON) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_AddItemReferenceToObject(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
        item: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_DetachItemViaPointer(parent: *mut cJSON, item: *mut cJSON) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_DetachItemFromArray(array: *mut cJSON, which: ::core::ffi::c_int)
        -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_DeleteItemFromArray(array: *mut cJSON, which: ::core::ffi::c_int);
}
extern "C" {
    pub fn cJSON_DetachItemFromObject(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_DetachItemFromObjectCaseSensitive(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_DeleteItemFromObject(object: *mut cJSON, string: *const ::core::ffi::c_char);
}
extern "C" {
    pub fn cJSON_DeleteItemFromObjectCaseSensitive(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
    );
}
extern "C" {
    pub fn cJSON_InsertItemInArray(
        array: *mut cJSON,
        which: ::core::ffi::c_int,
        newitem: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_ReplaceItemViaPointer(
        parent: *mut cJSON,
        item: *mut cJSON,
        replacement: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_ReplaceItemInArray(
        array: *mut cJSON,
        which: ::core::ffi::c_int,
        newitem: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_ReplaceItemInObject(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
        newitem: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_ReplaceItemInObjectCaseSensitive(
        object: *mut cJSON,
        string: *const ::core::ffi::c_char,
        newitem: *mut cJSON,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_Duplicate(item: *const cJSON, recurse: cJSON_bool) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_Compare(
        a: *const cJSON,
        b: *const cJSON,
        case_sensitive: cJSON_bool,
    ) -> cJSON_bool;
}
extern "C" {
    pub fn cJSON_Minify(json: *mut ::core::ffi::c_char);
}
extern "C" {
    pub fn cJSON_AddNullToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddTrueToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddFalseToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddBoolToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
        boolean: cJSON_bool,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddNumberToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
        number: f64,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddStringToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
        string: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddRawToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
        raw: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddObjectToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_AddArrayToObject(
        object: *mut cJSON,
        name: *const ::core::ffi::c_char,
    ) -> *mut cJSON;
}
extern "C" {
    pub fn cJSON_SetNumberHelper(object: *mut cJSON, number: f64) -> f64;
}
extern "C" {
    pub fn cJSON_SetValuestring(
        object: *mut cJSON,
        valuestring: *const ::core::ffi::c_char,
    ) -> *mut ::core::ffi::c_char;
}
extern "C" {
    pub fn cJSON_malloc(size: usize) -> *mut ::core::ffi::c_void;
}
extern "C" {
    pub fn cJSON_free(object: *mut ::core::ffi::c_void);
}
