use crate::{Json, Value};
use alloc::string::ToString;

#[test]
fn test_parse() {
    let s = r#"{
        "rc": 0,
        "rd": "ok",
        "data": {
            "api_gws": [
                {
                    "svr_domain": "cn.api.gw.iot.eprosmartlife.com",
                    "svr_ipv4": "121.37.248.22",
                    "svr_ipv6": "",
                    "priority": 0,
                    "protocols": "https;http"
                }
            ],
            "dev_gws": [
                {
                    "svr_domain": "",
                    "svr_ipv4": "121.37.248.22",
                    "svr_ipv6": "",
                    "priority": 0,
                    "protocols": "mqtt"
                },
                {
                    "svr_domain": "",
                    "svr_ipv4": "203.107.6.88",
                    "svr_ipv6": "",
                    "priority": 0,
                    "protocols": "ntp"
                }
            ]
        }
    }\0"#;
    let root = Json::parse(s.as_ptr() as *const i8).unwrap();
    let s = root.to_string();
    assert!(!s.is_empty());
    let rc = root
        .get("rc\0".as_ptr() as *const i8)
        .unwrap()
        .as_i64()
        .unwrap();
    let rd = root.get("rd\0".as_ptr() as *const i8);
    let rd = rd.as_ref().unwrap().as_str().unwrap();
    let data = root.get("data\0".as_ptr() as *const i8).unwrap();
    assert!(rc == 0);
    assert!(rd == "ok");
    assert!(!data.is_null());
    // println!("rc {} rd {}", rc, rd);
}

#[test]
fn test_build() {
    let mut json = Json::new();
    json.put("rc\0".as_ptr() as *const i8, Value::new_number(0.into()));
    json.put(
        "rd\0".as_ptr() as *const i8,
        Value::new_str("ok\0".as_ptr() as *const i8),
    );
    json.put("data\0".as_ptr() as *const i8, Value::new_object());
    let s = json.to_string();
    // unsafe { libc::printf("%s\n\0".as_ptr() as *const i8, s.as_ptr() as *const i8) };
    assert!(s.find("data").is_some());
}
