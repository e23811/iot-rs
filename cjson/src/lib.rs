#![no_std]
#![feature(negative_impls)]
#![allow(dead_code)]

extern crate alloc;

#[allow(non_snake_case, non_camel_case_types, non_upper_case_globals)]
#[allow(improper_ctypes)] //for 128-bit integers don't currently have a known stable ABI
pub mod ffi;
mod json;
#[cfg(test)]
mod json_test;
#[allow(non_upper_case_globals)]
mod value;

use crate::ffi::cJSON_Version;
pub use embed_std::{libc, Error, Result};
pub use json::Json;
pub use value::{RefValue, Value};

pub fn version() -> &'static str {
    unsafe {
        let v = cJSON_Version();
        alloc::str::from_utf8_unchecked(core::slice::from_raw_parts(
            v as *const u8,
            libc::strlen(v) as usize,
        ))
    }
}
