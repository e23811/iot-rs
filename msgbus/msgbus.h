//
// Created by armers on 20-3-26.
//

#ifndef MSGBUS_MSGBUS_H
#define MSGBUS_MSGBUS_H

#ifdef __cplusplus
extern "C" {
#endif
/// 0(off) 1(trace) 2(debug) 3(info) 4(warn) 5(error) others(info)
//void enable_log(int level);

typedef struct _MessageBusCallback {
    void (*handle)(const char *topic, const char *data, int data_len, void *args);
    // @param return_len: reponse data len
    // @return NEED to mallc, and caller need to call libc free
    char* (*call)(const char *topic, const char *method, const char *data, int data_len, void *args, int *return_len);
    void (*destroy)(void *args);
}MessageBusCallback;

struct MessageBus;

struct MessageBus *get_bus();
// @return token: NEED to free, used to unsubcribe
char* subscribe(struct MessageBus *bus, const char *topic, MessageBusCallback callback, void *args);
void unsubscribe(struct MessageBus *bus, const char *token);
void unsubscribe_topic(struct MessageBus *bus, const char *topic);
void publish(struct MessageBus *bus, const char *topic, const char *data, int data_len, int to_in_ms);
//@param out: reponse data, NEED to call free(xxx)
//@param out_len: reponse data len
//@return 0 ok, !0 failed
int call(struct MessageBus *bus, const char *topic, const char *method, const char *args, int args_len, char **out, int *out_len, int to_in_ms);

#ifdef __cplusplus
}
#endif

#endif //MSGBUS_MSGBUS_H
