use crate::message::{CallReturn, Message};

pub trait MessageHandler: Sync + Send {
    fn handle(&self, msg: &Message);
    fn call(&self, topic: &str, method: &str, args: &[u8]) -> CallReturn;
}
