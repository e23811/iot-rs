fn main() {
    cc::Build::new()
        .flag("-ffunction-sections")
        .flag("-fdata-sections")
        .file("c/lib.cpp")
        .compile("liblib.a");
    println!("cargo:rustc-link-lib=static=lib");
}