#ifndef MSGBUS_MSGBUS_HPP
#define MSGBUS_MSGBUS_HPP

#include <string>

namespace msgbus {
    class MsgHandler {
    public:
        MsgHandler();
        MsgHandler(bool auto_free);
        virtual ~MsgHandler();

        bool need_free() const { return auto_free_; }
    public:
        virtual void handle(const char *topic, const char *data, int data_len) { }
        // @param return_len: reponse data len
        // @return NEED to mallc, and caller need to call libc free
        virtual bool call(const char *topic, const char *method, const char *data, int data_len, std::string *out_data) { return false; }


    private:
        bool auto_free_;
    };

    class MsgBus {
    protected:
        MsgBus();
    public:
        virtual ~MsgBus() {}

    public:
        static MsgBus* inst();
        /// 0(off) 1(trace) 2(debug) 3(info) 4(warn) 5(error) others(info)
//        void enable_log(int level);
        // @param handler: need new, freed when unsubscribe
        // @return subscribe token
        std::string subscribe(const char *topic, MsgHandler *handler);
        void unsubscribe(const char *token);
        void unsubscribe_topic(const char *topic);
        void publish(const char *topic, const char *data, int data_len, int to_in_ms);
        bool call(const char *topic, const char *method, const char *args, int args_len, std::string *out_data, int to_in_ms);
    private:
        void* msgbus_;
    };
}


#endif //MSGBUS_MSGBUS_HPP
