#!/bin/bash


. ~/bin/t31.sh
export TARGET_CC=mips-linux-uclibc-gnu-gcc
export TARGET_AR=mips-linux-uclibc-gnu-ar
export SDK_DIR=/home/armers/projects/embed/t31/ipc_sdk
export OPENSSL_DIR=$SDK_DIR
# 需要修改libcurl-sys的build.rs, 支持环境变量查找
export LIBCURL_LIB_DIR=$SDK_DIR/lib

xargo build -Z build-std --release --target mipsel-unknown-linux-uclibc -vvv
