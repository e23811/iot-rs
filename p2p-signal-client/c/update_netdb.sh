#!/bin/bash

TARGET="../src/netdb/ffi.rs"
echo "use core::prelude::rust_2018::derive;" > $TARGET
echo "use core::clone::Clone;" >> $TARGET
echo "use core::marker::Copy;" >> $TARGET
bindgen --use-core --no-layout-tests --no-derive-debug -o bindings.rs net_wrapper.h

sed -i s/std::os::raw::/core::ffi::/g bindings.rs
cat bindings.rs >> $TARGET
rm -f bindings.rs
