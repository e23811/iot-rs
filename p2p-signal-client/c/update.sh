#!/bin/bash

TARGET="../src/ffi.rs"
echo "use core::prelude::rust_2018::derive;" > $TARGET
echo "use core::clone::Clone;" >> $TARGET
echo "use core::marker::Copy;" >> $TARGET
bindgen --use-core --no-layout-tests --no-derive-debug -o bindings.rs signal_client.h

sed -i s/std::os::raw::/core::ffi::/g bindings.rs
cat bindings.rs >> $TARGET
rm -f bindings.rs
