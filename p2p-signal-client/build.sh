#!/bin/bash

case "$1" in
  ax)
    . ~/bin/ax.sh
    export LIBCURL_LIB_DIR=~/projects/embed/IOT-AU38-01/app/opal_app/app/src/tutk_lib
    export OPENSSL_DIR=~/projects/embed/IOT-AU38-01/third_sdk/openssl
    TARGET="armv7-unknown-linux-gnueabihf"
    ;;
  jz)
    . ~/bin/t31.sh
    export LIBCURL_LIB_DIR=~/projects/embed/t31/ipc_sdk/lib
    export OPENSSL_INCLUDE_DIR=~/projects/embed/t31/ipc_sdk/include
    export OPENSSL_LIB_DIR=~/projects/embed/t31/ipc_sdk/lib
    TARGET="mipsel-unknown-linux-uclibc"
    ;;
  x86)
  	TARGET="x86_64-unknown-linux-gnu"
    ;;
  *)
    echo "Usage $0 <platform>"
    echo "    platform: < x86 | ax >"
    exit 1
esac

#xargo build -Z build-std --release --target mipsel-unknown-linux-uclibc
cargo xbuild --release -Z build-std -Z build-std-features=panic_immediate_abort --target $TARGET
