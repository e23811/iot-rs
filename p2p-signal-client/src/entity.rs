use crate::ffi::{
    signal_msg_id_t_smi_remote_ice_candidate_update, signal_msg_id_t_smi_remote_sdp_arrived,
    signal_msg_id_t_smi_session_closing, signal_msg_id_t_smi_session_created,
    signal_msg_id_t_smi_session_created_by_remote, signal_msg_id_t_smi_session_error,
    signal_msg_id_t_smi_session_status,
};
use embed_std::errorln;

#[derive(Debug)]
pub enum RequestCmd {
    CreateSession,
    GetSessionStatus,
    ControlSession,
    UpdateSdp,
    UpdateIceCands,
    ReportStatus,
}
impl From<RequestCmd> for u8 {
    fn from(v: RequestCmd) -> Self {
        match v {
            RequestCmd::CreateSession => 1,
            RequestCmd::GetSessionStatus => 2,
            RequestCmd::ControlSession => 3,
            RequestCmd::UpdateSdp => 4,
            RequestCmd::UpdateIceCands => 5,
            RequestCmd::ReportStatus => 6,
        }
    }
}

#[derive(Debug)]
pub enum ControlSessionStatus {
    // 1: 接受建立会话，
    // 2：会话错误，
    // 3：关闭会话
    // 4:  P2P通讯建立
    Accepted,
    HasError,
    RequestToClose,
    P2pConnected,
}

impl From<ControlSessionStatus> for u8 {
    fn from(v: ControlSessionStatus) -> Self {
        match v {
            ControlSessionStatus::Accepted => 1,
            ControlSessionStatus::HasError => 2,
            ControlSessionStatus::RequestToClose => 3,
            ControlSessionStatus::P2pConnected => 4,
        }
    }
}

#[derive(Debug)]
pub enum SessionNotifyCmd {
    Created,
    CreatedRequest,
    StartP2p,
    RequestToClose,
    ReportStatus,
    HasError,
    AnswerAccepted,
    UpdateIceCands,
}

impl From<u8> for SessionNotifyCmd {
    fn from(v: u8) -> Self {
        match v {
            1 => SessionNotifyCmd::Created,
            2 => SessionNotifyCmd::CreatedRequest,
            3 => SessionNotifyCmd::StartP2p,
            4 => SessionNotifyCmd::RequestToClose,
            5 => SessionNotifyCmd::ReportStatus,
            6 => SessionNotifyCmd::HasError,
            7 => SessionNotifyCmd::AnswerAccepted,
            8 => SessionNotifyCmd::UpdateIceCands,
            n => {
                errorln!("session notify cmd {} unknown, assume error", n);
                SessionNotifyCmd::HasError
            }
        }
    }
}

#[derive(Debug)]
pub enum SignalMsgId {
    SessionCreated,
    SessionCreatedByRemote,
    RemoteSdpArrived,
    SessionClosing,
    RemoteIceCandidateUpdate,
    SessionError,
    SessionStatus,
}

impl From<u32> for SignalMsgId {
    fn from(v: u32) -> Self {
        match v {
            signal_msg_id_t_smi_session_created => SignalMsgId::SessionCreated,
            signal_msg_id_t_smi_session_created_by_remote => SignalMsgId::SessionCreatedByRemote,
            signal_msg_id_t_smi_remote_sdp_arrived => SignalMsgId::RemoteSdpArrived,
            signal_msg_id_t_smi_session_status => SignalMsgId::SessionStatus,
            signal_msg_id_t_smi_session_closing => SignalMsgId::SessionClosing,
            signal_msg_id_t_smi_remote_ice_candidate_update => {
                SignalMsgId::RemoteIceCandidateUpdate
            }
            signal_msg_id_t_smi_session_error => SignalMsgId::SessionError,
            n => {
                errorln!("signal id {} unknown!", n);
                SignalMsgId::SessionError
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum SessionStatus {
    // 0: 已创建
    // 1: 创建会话等待answer,
    // 2：会话已接受，
    // 3：会话错误，
    // 4：启动P2P通讯，
    // 5: 会话关闭，
    // 6：会话进行中
    Created,
    WaitForAnswer,
    Accepted,
    HasError,
    StartP2p,
    Closed,
    Running,
}

impl From<u8> for SessionStatus {
    fn from(i: u8) -> Self {
        match i {
            0 => SessionStatus::Created,
            1 => SessionStatus::WaitForAnswer,
            2 => SessionStatus::Accepted,
            3 => SessionStatus::HasError,
            4 => SessionStatus::StartP2p,
            5 => SessionStatus::Closed,
            6 => SessionStatus::Running,
            n => {
                errorln!("status {} unknown", n);
                SessionStatus::HasError
            }
        }
    }
}

impl From<SessionStatus> for u8 {
    fn from(value: SessionStatus) -> Self {
        match value {
            SessionStatus::Created => 0,
            SessionStatus::WaitForAnswer => 1,
            SessionStatus::Accepted => 2,
            SessionStatus::HasError => 3,
            SessionStatus::StartP2p => 4,
            SessionStatus::Closed => 5,
            SessionStatus::Running => 6,
        }
    }
}
