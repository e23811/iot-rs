use core::ptr::copy;

pub fn copy_str_to_slice(src: &str, dst: &mut [u8]) {
    let len = if src.len() > dst.len() {
        dst.len()
    } else {
        src.len()
    };
    unsafe {
        copy(src.as_ptr(), dst.as_mut_ptr(), len);
    }
}

pub fn c_slice_as_str(src: &[i8]) -> &str {
    unsafe {
        let s: &[u8] = core::mem::transmute(src);
        alloc::str::from_utf8_unchecked(s)
    }
}
