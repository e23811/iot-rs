#![no_std]
#![allow(dead_code)]

#[allow(unused)]
mod capi;
mod compact;
#[allow(non_upper_case_globals)]
mod entity;
#[allow(non_upper_case_globals)]
#[allow(unused_variables)]
#[allow(non_camel_case_types)]
pub mod ffi;
pub mod netdb;
pub mod rapi;
pub mod url;
pub mod util;
mod wsclient;

extern crate alloc;

use crate::ffi::signal_client_opt_t;
use crate::util::copy_str_to_slice;
pub use capi::create_client;

pub struct SignalClientOption<'a> {
    pub url: &'a str,
    pub user: &'a str,
    pub pwd: &'a str,
}

impl<'a> From<SignalClientOption<'a>> for signal_client_opt_t {
    fn from(s: SignalClientOption) -> Self {
        let mut opt: signal_client_opt_t = unsafe { core::mem::zeroed() };
        copy_str_to_slice(s.url, unsafe { core::mem::transmute(&mut opt.url[..]) });
        copy_str_to_slice(s.user, unsafe { core::mem::transmute(&mut opt.user[..]) });
        copy_str_to_slice(s.pwd, unsafe { core::mem::transmute(&mut opt.pwd[..]) });
        opt
    }
}

type Result<T> = core::result::Result<T, ffi::signal_error_e>;
pub use entity::SignalMsgId;
