#[cfg(target_arch = "mips")]
use embed_std::libc;

//MIPS32 not support, so patch it
#[cfg(target_arch = "mips")]
#[no_mangle]
extern "C" fn pthread_setname_np(_thread_id: libc::pthread_t, _name: *mut libc::c_char) {
    use embed_std::warnln;
    warnln!("pthread_setname_np not support");
}
