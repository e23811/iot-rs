extern crate alloc;
extern crate core;

#[allow(non_camel_case_types)]
#[allow(non_upper_case_globals)]
pub mod capi;

pub mod low_energy_client;
