#ifndef IOT_LECLIENT_H
#define IOT_LECLIENT_H

typedef enum {
    LE_STATUS_NORMAL,
    LE_STATUS_SLEEPING,
    LE_STATUS_WAKING,
    LE_STATUS_WOKE,
    LE_STATUS_TO_WAKE,
} low_energy_status_e;

struct low_energy_client {
    void *priv;
    void (*free)(struct low_energy_client *self);
    /**
     * @return 0(ok), !0(fail)
     */
    int (*init)(struct low_energy_client *self);
    /**
     * @return 0(ok), !0(fail)
     */
    int (*report)(struct low_energy_client *self, low_energy_status_e status);
    int (*handle_msg)(struct low_energy_client *self, unsigned int to_in_ms);
};

typedef enum {
    LOW_ENERGY_EVENT_AWAKE
} low_energy_event_e;

typedef void (* low_energy_event_func_t)(low_energy_event_e type);

typedef struct {
    //seperated by semicolon.  ip:port;ip:port
    char servers[128];
    char device_uid[64];
    char product_code[64];
    char secret[64];
    low_energy_event_func_t event_cb;
} low_energy_client_option_t;

struct low_energy_client* create_le_client(low_energy_client_option_t *opt);

#endif //IOT_LECLIENT_H
