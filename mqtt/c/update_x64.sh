#!/bin/bash

ARCH="x86-64"
FFI="ffi-$ARCH.rs"
rm -f $FFI
echo "use core::prelude::rust_2018::derive;">>$FFI
echo "use core::clone::Clone;">>$FFI
echo "use core::marker::Copy;">>$FFI
bindgen --no-layout-tests wrapper.h -- -DLINUX=1 -ffunction-sections -fdata-sections >>$FFI
FFI_TEMP=".$FFI"
sed 's/std::os::raw::/core::ffi::/g' $FFI > $FFI_TEMP
sed 's/::std::/::core::/g' $FFI_TEMP > $FFI
rm -f $FFI_TEMP
cp $FFI ../src/ffi.rs
