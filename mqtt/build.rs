use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;

#[allow(dead_code)]
fn modify(filename: PathBuf) -> std::io::Result<()> {
    let mut content = String::new();
    content.push_str("use core::prelude::rust_2018::derive;\n");
    content.push_str("use core::clone::Clone;\n");
    content.push_str("use core::marker::Copy;\n");

    let mut f = File::open(filename.as_path())?;
    let mut origin = String::new();
    let size = f.read_to_string(&mut origin)?;
    println!("read file size {}", size);
    origin = origin.replace("std::os::raw::", "core::ffi::");
    origin = origin.replace("::std::", "::core::");
    content.push_str(origin.as_str());

    let mut f1 = File::create(filename.as_path())?;
    f1.write_all(content.as_bytes())?;
    Ok(())
}

#[cfg(feature = "paho")]
fn paho_lib() {
    use std::env;
    let target = env::var("TARGET").expect("TARGET was not set");
    let os_type = if target.contains("linux") {
        "LINUX"
    } else {
        panic!("WARNING: {} not support!", target);
    };
    let base_dir = format!("{}/c", env!("CARGO_MANIFEST_DIR"));
    let mut builder = cc::Build::new();
    builder.include(base_dir.as_str());
    for sf in vec![
        "MQTTClient.c",
        "MQTTConnectClient.c",
        "MQTTConnectServer.c",
        "MQTTDeserializePublish.c",
        "MQTTFormat.c",
        "MQTTPacket.c",
        "MQTTSerializePublish.c",
        "MQTTSubscribeClient.c",
        "MQTTSubscribeServer.c",
        "MQTTUnsubscribeClient.c",
        "MQTTUnsubscribeServer.c",
        "MQTTLiteOS.c",
    ] {
        builder.file(format!("{}/{}", base_dir, sf));
    }
    builder.define(os_type, "1");
    builder.compile("mqtt_core");
}

fn main() {
    #[cfg(feature = "paho")]
    paho_lib()
}
